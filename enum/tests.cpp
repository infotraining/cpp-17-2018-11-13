#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

enum Engine : uint8_t { diesel, petrol, hybrid };

enum class Coffee : uint8_t { espresso, dopio, cappucino };

TEST_CASE("enums with direct init")
{
	Engine e1{ 2 };
	REQUIRE(e1 == hybrid);

	Coffee c{ 1 };
	REQUIRE(c == Coffee::dopio);
}

enum class Index : size_t
{};

constexpr size_t to_size_t(Index i)
{
	return static_cast<size_t>(i);
}

Index operator+(Index i, size_t offset)
{
	return Index{ to_size_t(i) + offset };
}

struct Array
{
	vector<int> items = { 1, 2, 3, 4, 5 };

	int& operator[](Index index)
	{
		return items[to_size_t(index)];
	}
};

TEST_CASE("using Index with Array")
{
	Array tab;

	REQUIRE(tab[Index{ 2 }] == 3);
}

struct Data
{
	explicit Data(int size_t) {}
	Data(initializer_list<int> il) {}
};

void(*ptr)() noexcept;

void foo1() noexcept
{}

void foo2() throw()
{
}

void use() noexcept
{
	foo1();
}

template <typename T>
void my_swap(T& a, T& b) noexcept(noexcept(a.swap(b)))
{
	a.swap(b);
}

TEST_CASE("auto bug fixed")
{
	int x1(); // ERROR
	int x2(4); // x2 == 4
	int x3{ 3 }; // x3 == 3

	auto x4{ 5 }; // C++11 - initializer_list<int>{5} - since C++17 int
	auto x5 = { 5 }; // initializer_list<int>{5}
	//auto x6{ 1, 2, 3, 4 }; // ill-formed since C++17

	SECTION("Beware")
	{
		auto lst{ 1 };
		Data d{ lst };
	}

	ptr = &foo1;
	ptr = &foo2;

	static_assert(noexcept(foo2()));
}