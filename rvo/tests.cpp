#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"
#include <future>

using namespace std;

vector<int> create_vector(size_t size)
{
	return vector<int>(size, -1); // Return Value Optimization (RVO)
}

vector<int> load_data()
{
	vector<int> vec;
	vec.reserve(1'000'000);

	for (int i = 0; i < 10; ++i)
		vec.push_back(i);

	return vec; // Named Return Value Optimization (NRVO) 
}

void print(vector<int> vec)
{
	for(const auto& item : vec)
	{
		cout << item << " ";
	}
	cout << '\n';
}

TEST_CASE("rvo")
{
	vector<int> vec = create_vector(1'000'000); // optimized by RVO

	vector<int> data = load_data(); // maybe optimized by NRVO

	print(vector<int>{1, 2, 3, 4}); // no copy or move (RVO)
}

struct OnlyMoveable
{
	string value;

	OnlyMoveable(const OnlyMoveable&) = delete;
	OnlyMoveable& operator=(const OnlyMoveable&) = delete;
	OnlyMoveable(OnlyMoveable&&) = default;
	OnlyMoveable& operator=(OnlyMoveable&&) = default;
};

struct NoCopyableAndMoveable
{
	string value;

	NoCopyableAndMoveable(const NoCopyableAndMoveable&) = delete;
	NoCopyableAndMoveable& operator=(const NoCopyableAndMoveable&) = delete;
};

NoCopyableAndMoveable foo()
{
	return NoCopyableAndMoveable{ "no copy & move" }; // RVO
}

OnlyMoveable foo_with_move()
{
	OnlyMoveable result{ "move" };

	return result; // maybe NRVO
}

void use(NoCopyableAndMoveable v) 
{
	cout << v.value << endl;
}

TEST_CASE("NoCopyable")
{
	NoCopyableAndMoveable nc1 = foo(); // OK - RVO

	OnlyMoveable om = foo_with_move(); // NRVO

	REQUIRE(om.value == "move"s);

	// use(NoCopyableAndMoveable{ "c++17" }); // it should compile in C++17

	NoCopyableAndMoveable{ "temp" }.value;	
}

[[nodiscard]] int load_file(const char* filename)
{
	return 13;
}

TEST_CASE("attributes")
{
	[[maybe_unused]] int x = 13;

	if (int result = load_file("bad-file"); result == 0)
	{
		cout << "OK" << endl;
	}
	else
	{
		cout << "bad things happened" << endl;
	}	
}

TEST_CASE("async bug - with [[nodiscard]] is safer")
{
	async(launch::async, load_file, "1");
	async(launch::async, load_file, "2");
	async(launch::async, load_file, "3");
}

