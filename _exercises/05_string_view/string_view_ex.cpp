#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <string_view>
#include <vector>

#include "catch.hpp"

using namespace std;

vector<string_view> split_text(string_view text, string_view delims = " ,"sv)
{
	vector<string_view> tokens;

	auto it1 = cbegin(text);
	
	while(it1 != end(text))
	{
		auto it2 = find_first_of(it1, end(text), cbegin(delims), cend(delims));

		tokens.emplace_back(text.substr(it1 - cbegin(text), it2 - it1));

		if (it2 == end(text))
			break;

		it1 = next(it2);
	}

	return tokens;
}

TEST_CASE("split with spaces")
{
    string text = "one two three four";

    auto words = split_text(text);

    auto expected = {"one", "two", "three", "four"};

    REQUIRE(equal(begin(expected), end(expected), begin(words)));
}

TEST_CASE("split with commas")
{
    string text = "one,two,three,four";

    auto words = split_text(text);

    auto expected = {"one", "two", "three", "four"};

    REQUIRE(equal(begin(expected), end(expected), begin(words)));
}


