#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

class Customer
{
private:
    std::string first_;
    std::string last_;
    uint8_t age_;

public:
    Customer(std::string f, std::string l, uint8_t age)
        : first_(std::move(f))
        , last_(std::move(l))
        , age_(age)
    {
    }
    std::string first() const
    {
        return first_;
    }
    std::string last() const
    {
        return last_;
    }
    uint8_t age() const
    {
        return age_;
    }
};

template <>
struct std::tuple_size<Customer> : std::integral_constant<size_t, 2>
{};

template <>
struct std::tuple_element<0, Customer> : std::common_type<string>
{};

template <>
struct std::tuple_element<1, Customer> : std::common_type<string>
{};

template <size_t>
auto get(const Customer& c);

template <>
auto get<0>(const Customer& c)
{
	return c.first();
}

template <>
auto get<1>(const Customer& c)
{
	return c.last();
}

TEST_CASE("decomposition using structured binding")
{
    Customer c("Jan", "Kowalski", 42);

     auto [f, l] = c;

     REQUIRE(f == "Jan");
     REQUIRE(l == "Kowalski");    
}