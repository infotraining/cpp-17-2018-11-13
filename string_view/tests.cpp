#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <string_view>

#include "catch.hpp"

using namespace std;

string_view get_token()
{
	auto text = "ab ba";

	return string_view(&text[0], 2);
}

TEST_CASE("string_view")
{
	SECTION("construction")
	{
		string_view sv1 = "text";

		string text = "abbbbbba";
		string_view text_sv = text;

		string_view part_sv(text.c_str(), 3);
		REQUIRE(part_sv == "abb"sv);

		SECTION("Beware")
		{
			string_view bad_sv = "text"s; // dangling pointer

			// cout << bad_sv << endl; // UB
		}
	}

	SECTION("remove_prefix")
	{
		string_view text = "__text__";

		text.remove_prefix(2);
		text.remove_suffix(2);

		REQUIRE(text == "text"sv);
	}
}

template <typename Container>
void print_items(const Container& c, string_view prefix = "items: ")
{
	cout << prefix;
	for (const auto& item : c)
		cout << item << " ";
	cout << '\n';
}

TEST_CASE("printing items")
{
	const char* title1 = "Items of container: ";
	string title2 = ">>> ";

	print_items(vector{ 1, 2, 3 }, title1);
	print_items(vector{ 1, 2, 3 }, title2);

}