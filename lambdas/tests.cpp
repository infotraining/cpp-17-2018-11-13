#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>

#include "catch.hpp"
#include <functional>

using namespace std;

struct Gadget
{
	string id = "unknown";

	explicit Gadget(string id) : id{move(id)}
	{
		cout << "Gadget(" << this->id << " : " << this << ")\n";
	}

	Gadget(const Gadget& source) : id{source.id}
	{
		cout << "Gadget(cc: " << this->id << " : " << this << ")\n";
	}

	~Gadget()
	{
		cout << "~Gadget(" << this->id << " : " << this << ")\n";
	}

	auto report()
	{
		auto l1 = [*this]() { cout << "Hello from Gadget(" << id << ": " << ")\n"; };
		auto l2 = [=]() { cout << "Hello from Gadget(" << id << ": " << this << ")\n"; };
		auto l3 = [&]() { cout << "Hello from Gadget(" << id << ": " << this << ")\n"; };

		return l1;
	}
};

TEST_CASE("capture *this")
{
	function<void()> reporter;

	{
		Gadget g{ "ipad" };
		reporter = g.report();
	}

	reporter();
}

TEST_CASE("lambdas are constexpr")
{
	auto square = [](int x) constexpr {		
		return x * x; 
	};

	static_assert(square(8) == 64);

	int tab[square(8)] = {};

	static_assert(size(tab) == 64);
}

namespace Experimental
{
	template <typename Iter, typename Pred>
	constexpr Iter find_if(Iter first, Iter last, Pred pred)
	{
		Iter it = first;
		for (; it != last; ++it)
			if (pred(*it))
				return it;

		if (it == last)
			throw "item not found";
	}
}

TEST_CASE("constexpr find_if")
{
	constexpr array<int, 8> data { 1, 2, 3, 4, 5, 64, 665, 1024 };

	constexpr auto value = *Experimental::find_if(begin(data), end(data), [](int x) { return x > 225; });

	static_assert(value == 665);

	auto by_factor = [=](int f) constexpr { return f * value; };

	static_assert(by_factor(2) == 1330);
}