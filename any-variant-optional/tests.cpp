#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <any>
#include <variant>

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
	any ao;

	REQUIRE_FALSE(ao.has_value());

	ao = 1;
	ao = "text"s;
	ao = 3.14;

	vector vec{ 1, 2, 3 };

	ao = vec;

	SECTION("any_cast")
	{
		using namespace Catch::Matchers;

		auto data = any_cast<vector<int>>(ao);

		REQUIRE_THAT(data, Equals(vector{ 1, 2, 3 }));

		REQUIRE_THROWS_AS(any_cast<string>(ao), bad_any_cast);
	}

	SECTION("any_cast with pointer")
	{
		auto* ptr_vec = any_cast<vector<int>>(&ao);

		using namespace Catch::Matchers;
		REQUIRE_THAT(*ptr_vec, Equals(vector{ 1, 2, 3 }));

		auto* ptr_str = any_cast<string>(&ao);
		REQUIRE(ptr_str == nullptr);
	}

	SECTION("type")
	{
		cout << ao.type().name() << endl;
	}

	SECTION("emplace")
	{
		string& ref_o = ao.emplace<const string>("text");

		ref_o += "!";

		auto* ptr_str = any_cast<string>(&ao);

		REQUIRE(ptr_str != nullptr);
		REQUIRE(*ptr_str == "text!");		
	}	
}

TEST_CASE("---")
{
	cout << "\n\n------------------------------\n\n";
}

struct X
{
	int value;

	X() = delete;
};

struct Y
{
	int value;

	Y() = delete;
};

TEST_CASE("variant")
{
	SECTION("default construction")
	{
		variant<int, double, string, vector<int>> var;

		REQUIRE(std::holds_alternative<int>(var));

		auto& ref_int = get<int>(var);
		REQUIRE(ref_int == 0);
	}

	SECTION("monostate")
	{
		variant<monostate, X, Y> var;

		REQUIRE(std::holds_alternative<monostate>(var));

		var = X{ 42 };
		var = Y{ 665 };

		REQUIRE(get<Y>(var).value == 665);
		REQUIRE(var.index() == 2);

		REQUIRE_THROWS_AS(get<X>(var), bad_variant_access);
	}

	SECTION("access using index")
	{
		variant<int, double, int, string> var = "text"s;

		var.emplace<0>(42);
		var.emplace<double>(3.14);

		REQUIRE_THROWS_AS(get<0>(var), bad_variant_access);
	}
}

struct PrinterVisitor
{
	void operator()(int x) const
	{
		cout << "int(" << x << ")\n";
	}

	void operator()(double x) const
	{
		cout << "double(" << x << ")\n";
	}

	void operator()(const string& s) const
	{
		cout << "string(" << s << ")\n";
	}
};

template <typename... Ts>
struct Overloader : Ts...
{
	using Ts::operator()...;
};

template <typename... Ts>
Overloader(Ts...)->Overloader<Ts...>;

struct ErrorCode
{
	string msg;
	int errc;
};

[[nodiscard]] variant<string, ErrorCode> load_file(const string& fn)
{
	if (fn == "bad")
		return ErrorCode{ "bad file access", 13 };

	return "text"s;
}


TEST_CASE("visiting variant")
{
	variant<int, double, string> var = 3.14;

	visit(PrinterVisitor{}, var); // safe

	SECTION("inline visitor")
	{
		var = "text"s;

		auto printer = Overloader{
			[](int x) { cout << "int(" << x << ")\n"; },
			[](double x) { cout << "double(" << x << ")\n"; },
			[](const string& s) { cout << "string(" << s << ")\n"; }
		};

		visit(Overloader{
			[](int x) { cout << "int(" << x << ")\n"; },
			//[](double x) { cout << "double(" << x << ")\n"; },
			[](const string& s) { cout << "string(" << s << ")\n"; }
		}, var);
	}

	SECTION("error handler")
	{
		visit(Overloader{
			[](const string& text) { cout << "content of file: " << text << "\n"; },
			[](ErrorCode ec) { cout << "Error: " << ec.msg << "\n"; }
			}, load_file("bad"));
	}
}

