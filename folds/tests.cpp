#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
	template <typename T>
	auto sum(T arg)
	{
		return arg;
	}

	template <typename Head, typename... Tail>
	auto sum(Head head, Tail... tail)
	{
		return head + sum(tail...);
	}
}

// left - fold
template <typename... Args>
auto sum(Args&&... args)
{
	return (... + std::forward<Args>(args));
}

template <typename... Args>
auto sum_r(Args&&... args)
{
	return (std::forward<Args>(args) + ...);
}

TEST_CASE("Fold expressions")
{
	auto result = sum(1, 2, 3, 4, 5); // ((((1 + 2) + 3) + 4) + 5)	
	REQUIRE(result == 15);

	auto text = sum("text"s, "abc"s, "qwe"s);
	REQUIRE(text == "textabcqwe"s);
}

template <typename... Args>
void print_all(const Args&... args)
{
	((cout << args << " "), ...);
	cout << '\n';
}

template <typename Head, typename... Args>
void print_all_spaced(const Head& head, const Args&... args)
{
	auto spaced = [](const auto& arg)
	{
		cout << " ";
		return arg;
	};

	cout << head;
	(cout << ... << spaced(args)) << '\n';
}

TEST_CASE("print_all")
{
	print_all(1, 3.14, "text"s, "abc");
}