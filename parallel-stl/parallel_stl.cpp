#include <algorithm>
#include <cassert>
#include <cctype>
#include <chrono>
#include <execution>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <numeric>
#include <optional>
#include <random>
#include <string>

using namespace std;

template <typename F>
class Stoper
{
    std::string description_;
    std::chrono::high_resolution_clock::time_point start_;
    F func_;

public:
    Stoper(const std::string& description, F func)
        : description_{description}
        , func_{func}
    {
        start_ = std::chrono::high_resolution_clock::now();
        func_();
    }

    Stoper(const Stoper&) = delete;
    Stoper& operator=(const Stoper&) = delete;

    Stoper(Stoper&&) = default;
    Stoper& operator=(Stoper&&) = default;

    ~Stoper()
    {
        auto end = std::chrono::high_resolution_clock::now();

        auto time_interval = std::chrono::duration_cast<std::chrono::milliseconds>(end - start_);

        std::cout << description_ << " - time: " << time_interval.count() << "ms" << std::endl;
    }
};

template <typename F>
auto BENCHMARK(const std::string& description, F&& func)
{
    return Stoper<F>(description, std::forward<F>(func));
}

namespace fs = std::filesystem;

std::optional<std::string> load_file_content(const std::filesystem::path& filename)
{
    std::ifstream in(filename, std::ios::in | std::ios::binary);

    if (in)
    {
        std::string content;
        in.seekg(0, std::ios::end);
        content.resize(static_cast<size_t>(in.tellg()));
        in.seekg(0, std::ios::beg);
        in.read(&content[0], content.size());
        in.close();
        return content;
    }
    else
        return {};
}

template <typename ExecutionPolicy>
std::uintmax_t count_words(string_view text, ExecutionPolicy exec_policy)
{
    if (text.empty())
        return 0;

    auto is_word_beginning = [](auto left, auto right) {
        return std::isspace(left) && !std::isspace(right);
    };

    std::uintmax_t wc = (!std::isspace(text.front()) ? 1 : 0);
    wc += std::transform_reduce(exec_policy,
        text.begin(),
        text.end() - 1,
        text.begin() + 1,
        std::size_t(0),
        std::plus<>(),
        is_word_beginning);

    return wc;
}

int main()
{
    vector<int> vec(100'000'000);

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<int> rnd_distr(0, 100);

    generate(vec.begin(), vec.end(), [&] { return rnd_distr(rnd_gen); });

    long long sum1{};

    BENCHMARK("normal", [&]() {
        sum1 = accumulate(vec.begin(), vec.end(), 0LL);
    });

    std::cout << "sum1: " << sum1 << endl;

    long long sum2{};

    BENCHMARK("parallel", [&]() {
        sum2 = reduce(std::execution::par_unseq, vec.begin(), vec.end(), 0LL);
    });

    std::cout << "sum2: " << sum2 << endl;

    cout << "\n---------------------------------------\n";

    fs::path user_path = R"(d:\infotraining)";
    vector<fs::directory_entry> dir_entries(fs::recursive_directory_iterator(user_path), {});

    uint64_t total_size1{};

    BENCHMARK("size of directory", [&] {
        total_size1 = accumulate(dir_entries.begin(), dir_entries.end(), 0ULL,
            [](const auto& ts, const fs::directory_entry& de) {
                auto fsize = fs::is_regular_file(de) ? fs::file_size(de.path()) : 0u;
                return ts + fsize;
            });
    });

    std::cout << "total_size1: " << total_size1 << endl;

    uint64_t total_size2{};

    BENCHMARK("size of directory - parallel", [&] {
        vector<uint64_t> file_sizes(dir_entries.size());

        total_size2 = transform_reduce(
            std::execution::par,
            dir_entries.begin(), dir_entries.end(), 0ULL,
            std::plus<>{},
            [](const fs::directory_entry& de) { return fs::is_regular_file(de) ? fs::file_size(de.path()) : 0u; });
    });

    std::cout << "total_size2: " << total_size1 << endl;

    cout << "\n---------------------------------------\n";

    fs::path book{"./proust.txt"};

    auto content = load_file_content(book).value_or(""s);

    {
        uintmax_t word_count{};

        BENCHMARK("word count", [& content = as_const(content), &word_count] {
            word_count = count_words(content, std::execution::seq);
        });

        cout << "word_count: " << word_count << endl;
    }

    {
        uintmax_t word_count{};

        BENCHMARK("word count", [& content = as_const(content), &word_count] {
            word_count = count_words(content, std::execution::par_unseq);
        });

        cout << "word_count: " << word_count << endl;
    }

    cout << "\n---------------------------------------\n";

    {
        fs::path user_path = R"(d:\infotraining\code)";

        fs::recursive_directory_iterator rec_it{user_path};

        vector<fs::directory_entry> cpp_files;

        copy_if(begin(rec_it), end(rec_it), back_inserter(cpp_files), [](const fs::directory_entry& de) {            
            return de.path().extension() == ".cpp";
        });

        assert(all_of(cpp_files.begin(), cpp_files.end(), [](const auto& de) { return de.path().extension() == "cpp" }));

        cout << "Found: " << cpp_files.size() << " cpp files..." << endl;

        {
            std::uintmax_t total_words{};

            BENCHMARK("word count in cpp files", [&cpp_files, &total_words] {
                total_words = transform_reduce(std::execution::seq,
                    cpp_files.begin(), cpp_files.end(), 0ULL,
                    std::plus<>{},
                    [](const fs::directory_entry& f) {
                        optional file_content = load_file_content(f.path());

                        return (file_content ? count_words(*file_content, std::execution::seq) : 0);
                    });
            });

            cout << "Found " << total_words << " in cpp files...\n";
        }

        {
            std::uintmax_t total_words{};

            BENCHMARK("word count in cpp files - parallel", [&cpp_files, &total_words] {
                total_words = transform_reduce(std::execution::par,
                    cpp_files.begin(), cpp_files.end(), 0ULL,
                    std::plus<>{},
                    [](const fs::directory_entry& f) {
                        optional file_content = load_file_content(f.path());

                        return (file_content ? count_words(*file_content, std::execution::par_unseq) : 0);
                    });
            });

            cout << "Found " << total_words << " in cpp files...\n";
        }
    }

    std::system("PAUSE");
}
