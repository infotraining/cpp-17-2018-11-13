#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"
#include <cassert>
#include <queue>
#include <mutex>

using namespace std;

TEST_CASE("if with initializer")
{
	vector<int> vec = { 2, 234, 23, 665, 23, 645, 88 };
	
	if (auto pos = find(begin(vec), end(vec), 665); pos != end(vec))
	{
		cout << "Item " << *pos << " was found" << endl;
	}
	else
	{
		cout << "Item was not found...";
		assert(pos == end(vec));
	}
}

TEST_CASE("use case with mutex")
{
	queue<int> q;
	mutex q_mtx;

	SECTION("before C++17")
	{
		{
			lock_guard<std::mutex> lk{ q_mtx };

			if (!q.empty())
			{
				int value = q.front();
				q.pop();
			}
		} // it matters

		//...notify_all
	}

	SECTION("since C++17")
	{
		if(lock_guard lk{q_mtx}; !q.empty())
		{
			int value = q.front();
			q.pop();
		}

		//... notify_all
	}
}

TEST_CASE("Scoped Lock")
{
	mutex mtx1;
	timed_mutex mtx2;

	SECTION("before c++17")
	{
		unique_lock<mutex> lk1{ mtx1, defer_lock };
		unique_lock<timed_mutex> lk2{ mtx2, defer_lock };

		lock(lk1, lk2);

		// critical section
	}

	SECTION("since c++17")
	{
		scoped_lock lks{ mtx1, mtx2 };

		// critical section
	}
}







