#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct Reporter
{
	Reporter()
	{
		cout << "Reporter(" << this << ")\n";
	}

	~Reporter()
	{
		cout << "~Reporter(" << this << ")\n";
	}
};

struct SimpleAggregate
{
	int a;
	double b;
	int tab[3];
	Reporter* dynamic_array = new Reporter[1];

	void print() const
	{
		cout << "SimpleAggregate(" << a << ", " << b << ", " << "[ ";
		for (const auto& item : tab)
			cout << item << " ";
		cout << "], *: " << dynamic_array <<	")\n";
	}

	~SimpleAggregate()
	{
		cout << "Clean-up: " << dynamic_array << endl;
		delete[] dynamic_array;
	}
};

TEST_CASE("Aggregates")
{
	SECTION("arrays")
	{
		int tab[10] = { 1, 2, 3 };
		for (const auto& item : tab)
			cout << item << " ";
		cout << endl;
	}

	SECTION("direct init")
	{		
		SimpleAggregate sa1{ 42, 3.14, { 1 } };
		sa1.print();

		SimpleAggregate sa2{ 42 };
		sa2.print();
	}

	SECTION("init by copy syntax")
	{
		SimpleAggregate sa = { 665, 3.14, { 3, 4, 5 } };
		sa.print();
	}

	SECTION("clean up using {}")
	{
		SimpleAggregate sa{};

		sa.print();
	}
}

struct Cpp17Aggregate : SimpleAggregate
{
	std::string id;
};

struct InverseCpp17Aggregarte : string
{
	Cpp17Aggregate value;
};

TEST_CASE("C++17 aggregates")
{
	{
		Cpp17Aggregate agg{ {42, 3.14, { 1, 2, 3} }, "text"s };

		agg.print();
		REQUIRE(agg.id == "text"s);
	}
	cout << "\n\n";

	{
		InverseCpp17Aggregarte inv_agg{ {"text"s}, 42, 3.14, { 1, 2, 3 }, new Reporter[2] };
		inv_agg.value.print();
		REQUIRE(inv_agg.size() == 4);
	}

	cout << "\n\n";

	{
		InverseCpp17Aggregarte tab_agg[3] = { { {"text"s}, 42, 3.14, { 1, 2, 3 }, new Reporter[2] } };

		for (const auto& item : tab_agg)
		{
			cout << item.c_str() << " "; item.value.print();
		}
	}
}

struct WTF
{
	int x = 42;

	WTF() = delete;
};

TEST_CASE("WTF")
{
	WTF wtf = WTF{};

	REQUIRE(wtf.x == 42);
}

