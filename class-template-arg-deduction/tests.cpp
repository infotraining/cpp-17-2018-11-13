#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace Case1
{
	template <typename T1, typename T2>
	struct Data
	{
		T1 fst;
		T2 snd;

		Data(T1 arg1, T2 arg2) : fst{ std::move(arg1) }, snd{ std::move(arg2) }
		{
			cout << "Data<T1, T2>(" << fst << ", " << snd << ")\n";
		}

		using type1 = T1;
		using type2 = T2;
	};

	template <typename T>
	struct Data<T, T>
	{
		T fst;
		T snd;

		Data(T arg1, T arg2) : fst{ std::move(arg1) }, snd{ std::move(arg2) }
		{
			cout << "Data<T>(" << fst << ", " << snd << ")\n";
		}

		using type = T;
	};
}

TEST_CASE("Class Template Arg Deduction")
{
	SECTION("Before C++17")
	{
		Case1::Data<int, double> d{ 1, 3.14 };
	}

	SECTION("Since C++17")
	{
		using namespace Case1;

		Data d1{ 1, "text"s }; // Data<int, string>		
		static_assert(is_same_v<decltype(d1), Data<int, string>>);
		
		Data d2{ 1, 1 };
		static_assert(is_same_v<decltype(d2), Data<int, int>>);		

		int tab[10];
		Data d3{ tab, "text" };
		static_assert(is_same_v<decltype(d3), Data<int*, const char*>>);		
	}
}

void foo(int)
{	
}

template <typename T>
void deduce1(T arg)
{
	//puts(__FUNCSIG__);
}

template <typename T>
void deduce2(T& arg)
{
	//puts(__FUNCSIG__);
}

TEST_CASE("type deduction rules")
{
	SECTION("Case1")
	{
		int x = 10;
		int& ref_x = x;
		const int& cref_x = x;
		int tab[10];


		auto ax1 = 1; // int
		auto ax2 = ref_x; // int
		auto ax3 = cref_x; // int
		auto ax4 = tab; // int*
		auto ax5 = foo; // void(*)(int)

		deduce1(x);
		deduce1(ref_x);
		deduce1(cref_x);
		deduce1(tab);
		deduce1(foo);
	}

	cout << "\n\n";

	SECTION("Case2")
	{
		int x = 10;
		int& ref_x = x;
		const int& cref_x = x;
		int tab[10];


		//auto& ax1 = 1; // ERROR
		auto& ax2 = ref_x; // int&
		auto& ax3 = cref_x; // const int&
		auto& ax4 = tab; // int(&)[10]
		auto& ax5 = foo; // void(&)(int)

		//deduce2(1); // ERROR
		deduce2(x);
		deduce2(ref_x);
		deduce2(cref_x);
		deduce2(tab);
		deduce2(foo);
	}
}

namespace Case2
{
	template <typename T1, typename T2>
	struct Data
	{
		T1 fst;
		T2 snd;

		Data(const T1& arg1, const T2& arg2) : fst{ arg1 }, snd{ arg2 }
		{
			cout << "Data<T1, T2>(" << &fst << ", " << snd << ")\n";
		}
	};

	//deduction guide
	template <typename T1, typename T2>
	Data(T1, T2)->Data<T1, T2>;
}

TEST_CASE("Data - constructors with ref")
{
	using namespace Case2;

	int tab[10];

	Data d1{ tab, "text" };
	static_assert(is_same_v<decltype(d1), Data<int*, const char*>>);
	
	const vector<int> vec(1'000'000);	
	Data d2{ vec, "text"s };
	static_assert(is_same_v<decltype(d2), Data<vector<int>, string>>);	
}