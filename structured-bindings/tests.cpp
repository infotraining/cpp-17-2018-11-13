#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"
#include <array>
#include <map>
#include <set>

using namespace std;

namespace BeforeCpp17 {
	template <typename Container>
	tuple<int, int, double> calc_stats(const Container& data)
	{
		decltype(begin(data)) min_it, max_it;
		tie(min_it, max_it) = std::minmax_element(begin(data), end(data));

		double avg = std::accumulate(begin(data), end(data), 0.0) / size(data);

		return { *min_it, *max_it, avg };
	}
}

template <typename Container>
tuple<int, int, double> calc_stats(const Container& data)
{
	auto[min_it, max_it] = std::minmax_element(begin(data), end(data));

	double avg = std::accumulate(begin(data), end(data), 0.0) / size(data);

	return { *min_it, *max_it, avg };
}

TEST_CASE("Before C++17")
{
	vector<int> vec = { 5, 42, 662, 1, 423, 55, 665, 6 };

	auto result = calc_stats(vec);

	REQUIRE(get<0>(result) == 1);
	REQUIRE(get<1>(result) == 665);
	REQUIRE(get<2>(result) == Approx(232.375));

	int min, max;
	double avg;

	tie(min, max, avg) = calc_stats(vec);

	REQUIRE(max == 665);
}

struct Base
{
};

struct Date : Base
{
	int y;
	std::string m;
	int d;

	Date(int y, std::string m, int d)
		: y{ y }
		, m{ m }
		, d{ d }

	{
	}

	virtual std::string to_str() const
	{
		return to_string(y) + "/"s + m + "/"s + to_string(d);
	}

	virtual ~Date() = default;
};

auto get_date()
{
	return Date{ 2018, "Nov", 13 };
}

TEST_CASE("Structured bindings")
{
	vector<int> vec = { 5, 42, 662, 1, 423, 55, 665, 6 };

	SECTION("allow to unpack tuple")
	{
		auto[min, max, avg] = calc_stats(vec);

		REQUIRE(min == 1);
		REQUIRE(max == 665);
		REQUIRE(avg == Approx(232.375));
	}

	SECTION("allow to unpack struct")
	{
		auto[year, month, day] = get_date();

		REQUIRE(year == 2018);
		REQUIRE(month == "Nov"s);
		cout << get_date().to_str() << endl;
	}

	SECTION("allow to unpack static array")
	{
		auto get_coord = []() -> int(&)[3]
		{
			static int coord[] = { 1, 2, 3 };
			return coord;
		};

		auto[x, y, z] = get_coord();

		REQUIRE(x == 1);
		REQUIRE(y == 2);
	}

	SECTION("allow to unpack std::array")
	{
		std::array<int, 3> arr = { 1, 2, 3 };

		auto[x, y, z] = arr;

		REQUIRE(x == 1);
	}
}

struct AdvancedData
{
	int coord[3];
	Date d;
};

TEST_CASE("AdvancedData")
{
	AdvancedData ad{ { 1, 2, 3 }, Date { 2018, "Nov", 13 } };

	auto[tab, date] = ad;

	auto expected_tab = { 1, 2, 3 };
	REQUIRE(equal(begin(tab), end(tab), begin(expected_tab)));
	REQUIRE(date.m == "Nov"s);
}

TEST_CASE("Structured binding - how it works")
{
	vector data = { 1, 2, 3 };

	auto[min, max, avg] = calc_stats(data);

	SECTION("works like this")
	{
		auto unnamed_obj = calc_stats(data);
		auto& min = get<0>(unnamed_obj);
		auto& max = get<1>(unnamed_obj);
		auto& avg = get<2>(unnamed_obj);
	}
}

TEST_CASE("const, volatile, & with structured binding")
{
	vector data = { 1, 2, 3 };

	SECTION("const")
	{
		const auto[min, max, avg] = calc_stats(data);
	}

	SECTION("const &")
	{
		const auto&[min, max, avg] = calc_stats(data);
	}

	SECTION("&")
	{
		auto&[min, max, avg] = calc_stats(data); // non-standard Visual behaviour - ill-formed in std C++
	}

	SECTION("&&")
	{
		auto&& [min, max, avg] = calc_stats(data);

		max++;

		REQUIRE(max == 4);
	}

	SECTION("alignas - only first field is aligned to 16")
	{
		alignas(16) auto[min, max, avg] = calc_stats(data);
	}
}

TEST_CASE("USE CASES")
{
	SECTION("iteration over map")
	{
		map<int, string> dict = { {1, "one"}, {2, "two"}, {3, "three"} };

		for(const auto&[key, value] : dict)
		{
			cout << key << " - " << value << endl;
		}
	}

	SECTION("insert to container")
	{
		set<int> s = { 1, 2, 3 };		

		if (auto[pos, was_performed] = s.insert(4); was_performed)
			cout << "insert " << *pos << " was performed" << endl;
		else
			cout << *pos << " is already in set" << endl;
	}

	SECTION("init for many vars")
	{
		auto[x, y, is_ok] = tuple{ 1, 3.14, true };

		vector vec = { 1, 2, 3 };

		for (auto[i, it] = tuple{ 0, begin(vec) } ; i < size(vec); ++i, ++it)
		{
			cout << i << " - " << *it << endl;
		}
	}
}

/////////////////////////////
// bug in gcc?

decltype(auto) foo()
{
	auto[fst, snd] = make_pair(1, "text"s);

	return snd;
}

TEST_CASE("beware")
{
	auto text = foo();

	cout << text << endl;
}

TEST_CASE("foreach protocol")
{
	int container[] = { 1, 2, 3 };

	for(const auto& item : container)
	{
		cout << item << " ";
	}
	cout << '\n';
}

/////////////////////////////////////////////////////
// tuple protocol for structure binding

enum Something
{};

const map<int, string> something_desc = { {0, "some"}, {1, "thing"} };

template <>
struct std::tuple_size<Something> 
{
	static constexpr size_t value = 2;
};

template <>
struct std::tuple_element<0, Something> : std::common_type<int>
{};

template <>
struct std::tuple_element<1, Something> : std::common_type<const string&>
{};

template <size_t N>
decltype(auto) get(const Something& s)
{
	if constexpr (N == 0)
		return static_cast<int>(s);
	else
		return something_desc.at(static_cast<int>(s));
}

//template <>
//decltype(auto) get<0>(const Something& s)
//{
//	return static_cast<int>(s);
//}
//
//template <>
//decltype(auto) get<1>(const Something& s)
//{
//	return something_desc.at(static_cast<int>(s));
//}

TEST_CASE("structured bindings for Something")
{
	const auto&[value, desc] = Something{};

	REQUIRE(value == 0);
	REQUIRE(desc == "some"s);
}

const int& get_item(size_t index, const vector<int>& data)
{
	return data[index];
}

TEST_CASE("beware with returning refs")
{
	vector<int> vec = { 1, 2, 3, 4, 5 };
	auto& item = get_item(2, vec);

	REQUIRE(item == 3);
}

template <typename T>
struct IsPointer
{
	constexpr static auto value =
		[]() {
			if constexpr (std::is_pointer_v<T>)
				return true;
			else
				return false;
		}();
};

//template <typename T>
//struct IsPointer<T*>
//{
//	constexpr static bool value = true;
//};

TEST_CASE("IsPointer")
{
	static_assert(IsPointer<int*>::value);
}