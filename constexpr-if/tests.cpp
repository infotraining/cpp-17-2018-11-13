#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
string convert_to_string(T value)
{
	if constexpr (is_arithmetic_v<T>)
	{
		return to_string(value);
	}
	else if constexpr (is_same_v<T, string>)
	{
		return value;
	}
	else // implicitly convertible
	{		
		return string{ value };
	}
}

TEST_CASE("constexpr if")
{
	SECTION("as_string")
	{
		SECTION("numbers")
		{
			string txt = convert_to_string(4);
			REQUIRE(txt == "4"s);
		}

		SECTION("string")
		{
			string txt = convert_to_string("text"s);

			REQUIRE(txt == "text"s);
		}

		SECTION("implicitly converted type")
		{
			string txt = convert_to_string("text");

			REQUIRE(txt == "text"s);
		}
	}
}

template <typename T, auto N>
void process_array(T (&tab)[N])
{
	if constexpr (N<= 255)
	{
		cout << "Processing small array" << endl;
	}
	else
	{
		cout << "Processing large array" << endl;
	}
}

TEST_CASE("Processing arrays")
{
	int small[128];
	process_array(small);

	double large[1024];
	process_array(large);
}

namespace BeforeCpp17
{
	void print()
	{		
	}

	template <typename Head, typename... Tail>
	void print(const Head& head, const Tail&... args)
	{
		cout << head << endl;

		print(args...);
	}
}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... args)
{
	cout << head << endl;

	if constexpr(sizeof...(args) > 0)
	   print(args...);
}

TEST_CASE("print varaidic")
{
	print(1, 3.14, "text"s);
}

// hpp

class Bitmap
{
	struct BitmapImpl;
	std::unique_ptr<BitmapImpl> impl_;

public:
	Bitmap();
	~Bitmap();
};

// cpp

struct BitmapImpl
{
	vector<char> image;
};

Bitmap::~Bitmap() = default;


TEST_CASE("Bitmap")
{
	Bitmap bmp;
}
